"use strict";
let form = document.querySelector('.password-form');
let icon = document.querySelectorAll('.fa-eye');
let input = document.querySelectorAll('input');
let button = document.querySelector('button');
let error = document.createElement('span');
error.innerText = 'Потрібно ввести однакові значення';
error.style.color = 'red';
error.hidden = true;
button.before(error);
let i=0;
form.addEventListener('click', (event) => {
    if (event.target.closest('i')) {
        if (event.target.classList.contains('fa-eye')) {
            event.target.classList.replace('fa-eye', 'fa-eye-slash');
            let typeText = event.target.previousElementSibling;
            typeText.type = 'text';
        } else if (event.target.classList.contains('fa-eye-slash')) {
            event.target.classList.replace('fa-eye-slash', 'fa-eye');
            let typePassword = event.target.previousElementSibling;
            typePassword.type = 'password';
        }
    };
    if (event.target.closest('button')){
        event.preventDefault();
     if (input[i].value==input[i+1].value){
        error.hidden = true;
        setTimeout(()=>alert('You are welcome'));
     } else {
        error.hidden = false;
    }
    }
});
